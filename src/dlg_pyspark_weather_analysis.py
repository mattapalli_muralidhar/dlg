from pyspark.sql import SparkSession
from pyspark.sql import functions as f

spark = SparkSession.builder \
        .master("local") \
        .config("sql.shuffle.partitions", 3) \
        .appName("dlg_exercise") \
        .getOrCreate()

df = spark.read.format('parquet').load('C:\\py\\')
df = df.withColumn('WeatherDate', f.to_date(f.col('ObservationDate'), 'yyyy-MM-dd'))

# rename existing columns
df = ( 
      df.select('WeatherDate', 'ScreenTemperature', 'Region')
        .withColumnRenamed('WeatherDate', 'Hottest Day')
        .withColumnRenamed('ScreenTemperature', 'Hottest Temperature')
        .withColumnRenamed('Region','Hottest Region')
     )

# get hottest temperature
get_hottest_temperature = (
                           df.groupby().agg({"Hottest Temperature":"max"})
                             .withColumnRenamed('max(Hottest Temperature)','get_hottest_temperature')
                             .collect()
                          )

# get hottest temparature value from list
for rec in get_hottest_temperature:
  hottest_temperature = rec['get_hottest_temperature']

# fire query using hottest temparature to get all hottest day, region, temparature  
(df.select('Hottest Day', 'Hottest Temperature', 'Hottest Region')
   .filter(f.col('ScreenTemperature') == hottest_temperature)
   .show())
