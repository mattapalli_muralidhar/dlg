import pandas as pd
from pathlib import Path
import sys

def weather_analysis(input_parquet_dir):

    # concats multiple parquet files    
    data_dir = Path(input_parquet_dir)
    df = pd.concat(pd.read_parquet(parquet_file) for parquet_file in data_dir.glob('*.parquet'))    

    df["WeatherDate"]=pd.to_datetime(df["ObservationDate"], format = '%Y-%m-%d')
    df["WeatherDate"]=df["WeatherDate"].dt.date
    df = df.loc[:,["WeatherDate", "ScreenTemperature", "Region"]]

    df = df.nlargest(1,'ScreenTemperature')
    
    hottest_date = df["WeatherDate"]
    hottest_temperature = df["ScreenTemperature"]
    hottest_region = df['Region']
    
    print("\n")
    print(" Hottest Day                    :", hottest_date.to_string(index=False, header=None))
    print(" Hottest Temperature            :", hottest_temperature.to_string(index=False, header=None))
    print(" Hottest Region                 :", hottest_region.to_string(index=False, header=None))

if __name__ == "__main__":
    # windows path but can be configurable for Unix through command line argument
    input_parquet_dir=sys.argv[1]
    
    # convert to parquet
    weather_analysis(input_parquet_dir)