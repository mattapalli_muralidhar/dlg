import pandas as pd
import pyarrow as pa
import pyarrow.parquet as pq
from datetime import datetime
from pathlib import Path
import sys

# get data from git
data_urls=['https://bitbucket.org/mattapalli_muralidhar/dlg/raw/2b00e650b4a413b13892b44130b74af6d4519b8c/data/weather.20160201.csv',
           'https://bitbucket.org/mattapalli_muralidhar/dlg/raw/2b00e650b4a413b13892b44130b74af6d4519b8c/data/weather.20160301.csv']

'''
pyarrow document reference:
 https://arrow.apache.org/docs/python/parquet.html

chunkSize reference - 
  kilobytes = 1024
  megabytes = kilobytes * 100
  chunkSize = int(10 * megabytes)
'''
chunkSize = int(sys.argv[1])

def convert_csv_to_parquet(output_parquet_dir):

    for data_file in data_urls:
        csv_data = pd.read_csv(data_file, sep=',', chunksize=chunkSize)
        print("Loading data file : ", data_file)

        for i, chunk in enumerate(csv_data):
            if i == 0:
                # build schema
                parquet_schema = pa.Table.from_pandas(df=chunk).schema

                # file for parquet writing
                parquet_writer = pq.ParquetWriter(output_parquet_dir + "weather." + data_file[(len(data_file)-12):len(data_file)-4] + ".parquet", parquet_schema, compression='snappy')
                print("Parquet file created on directory " + output_parquet_dir + " : ", "weather." + data_file[(len(data_file)-12):len(data_file)-4] + ".parquet")
                print("\n")
            
            # Write data to the parquet file
            table = pa.Table.from_pandas(chunk, schema=parquet_schema)
            parquet_writer.write_table(table)

        parquet_writer.close()

if __name__ == "__main__":
    # windows path but can be configurable for Unix, define the path below
    output_parquet_dir=sys.argv[2]
    
    # convert to parquet
    convert_csv_to_parquet(output_parquet_dir)
