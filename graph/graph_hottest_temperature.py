import pandas as pd
from datetime import datetime
from pathlib import Path

import numpy as np
import matplotlib.pyplot as plt
%matplotlib inline

# concats multiple parquet files    
data_dir = Path("C:\\py\\")
df = pd.concat(pd.read_parquet(parquet_file) for parquet_file in data_dir.glob('*.parquet'))
df["WeatherDate"]=pd.to_datetime(df["ObservationDate"], format = '%Y-%m-%d').astype(str)
df = df.nlargest(10,'ScreenTemperature')

plt.rcdefaults()
fig, ax = plt.subplots()

temperature = pd.Series(df["ScreenTemperature"]);
 
# Draw the bar chart
y_pos = np.arange(len(temperature))
ax.barh(y_pos, temperature, align='center')
ax.invert_yaxis()  # labels read top-to-bottom
ax.set_title('Hottest Temperature')
ax.set_xlabel('Screen Temperature')
ax.set_yticklabels([])

# tip values on top of bars
for i, v in enumerate(temperature):
    ax.text(v - 1.5, i, str(v), color='white')
plt.show()