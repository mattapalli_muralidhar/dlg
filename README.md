# README #



## Please use below table to navigate to different folder paths for relevent information ##

* __/(root)__     - Containing README.md, High Level Requirement
* __/dlg/data__	  - Contains CSV data-sets for the load process
* __/dlg/output__ -	Output from the load process is stored, in this case its parquet files
* __/dlg/src__	  - Python and PySpark source code for load process, analysis process and Jupyter file for graphs
* __/dlg/graph__  -	Jupyter Notebook in .py format for Graph analysis
* __/dlg/documents__ - Documents related to System Implementation, Unit Test Cases


## Below are the Weather Analysis requirement: ##
*	Which date was the hottest day?
*	What was the temperature on that day?
*	In which region was the hottest day?

